CREATE DATABASE lolchampion;

USE lolchampion;

CREATE TABLE IF NOT EXISTS `LolChampion` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `difficulty` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `dateCreated` datetime,
  `imageUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
