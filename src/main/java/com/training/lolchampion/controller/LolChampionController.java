package com.training.lolchampion.controller;

import com.training.lolchampion.entity.LolChampion;
import com.training.lolchampion.service.LolChampionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/v1/lolchampion")
@CrossOrigin("*")
public class LolChampionController {

    private static final Logger LOG = LoggerFactory.getLogger(LolChampionController.class);

    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public ResponseEntity<List<LolChampion>> findAll() {
        return new ResponseEntity<List<LolChampion>>(this.lolChampionService.findAll(), HttpStatus.OK);
    }

    /**
     * Find a shipper by unique id
     *
     * This example is demonstrating the use of a regex matcher in the @GetMapping annotation. In this case, this
     * method will be called for url paths that match /api/v1/shipper/X where X is a numerical value.
     *
     * This technique can be used to distinguish between paths that differ only by the type of variables contained.
     * However, this technique can lead to problems in certain cases, so should be used with caution. It may be safer
     * to distinguish paths by adding extra path elements rather than regex.
     */
    @GetMapping("/{id:[0-9]+}")
    public ResponseEntity<LolChampion> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<LolChampion>(lolChampionService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<LolChampion> save(@RequestBody LolChampion lolChampion) {
        LOG.debug("Request to create lolChampion [" + lolChampion + "]");
        try {
            return new ResponseEntity<>(lolChampionService.save(lolChampion), HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping
    public ResponseEntity<LolChampion> update(@RequestBody LolChampion lolChampion) {
        try {
            return new ResponseEntity<>(lolChampionService.update(lolChampion), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("update for unknown id: [" + lolChampion + "]");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        try {
            lolChampionService.delete(id);
        } catch(EmptyResultDataAccessException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
