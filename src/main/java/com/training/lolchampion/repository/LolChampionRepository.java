package com.training.lolchampion.repository;

import com.training.lolchampion.entity.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LolChampionRepository extends JpaRepository<LolChampion, Long>  {
}
