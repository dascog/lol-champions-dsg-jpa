package com.training.lolchampion.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.training.lolchampion.entity.LolChampion;
import jdk.jshell.spi.ExecutionControlProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class LolChampionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private String baseRestUrl = "/api/v1/lolchampion";

    @Test
    public void testFindAllSuccess() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get(baseRestUrl))
                                .andDo(print())
                                .andExpect(status().isOk())
                                .andReturn();
    }

    @Test
    public void testFindAllNotFound() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/champ"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testFindByIdSuccess() throws Exception
    {
        int testId = 1;

        MvcResult mvcResult = (MvcResult) this.mockMvc.perform( get(baseRestUrl+"/"+testId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // 3. verify the results - convert returned JSON to a single LolChampion object
        LolChampion champ = new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<LolChampion>() { });


        // verify the lolchampion object from the returned JSON has the requested id (1)
        assertThat(champ.getId()).isEqualTo(testId);
    }

    @Test
    public void testCreateLolChampion() throws Exception {
        String testDifficulty = "Hardest";
        String testName = "Fortran";
        String testRole = "Developer";
        Date testDate = new Date();
        String testImageUrl = "https://nourl/img.jpg";

        LolChampion testChamp = new LolChampion();
        testChamp.setDifficulty(testDifficulty);
        testChamp.setName(testName);
        testChamp.setRole(testRole);
        testChamp.setDateCreated(testDate);
        testChamp.setImageUrl(testImageUrl);

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(testChamp);

        MvcResult mvcResult = this.mockMvc.perform(post(baseRestUrl)
                .header("Content-Type","application/json")
                .content(requestJson))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();

        LolChampion champ =  new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<LolChampion>() {
                });

        assertThat(champ.getId()).isGreaterThan(0);
        assertThat(champ.getDateCreated()).isEqualTo(testDate);
        assertThat(champ.getName()).isEqualTo(testName);
        assertThat(champ.getRole()).isEqualTo(testRole);
        assertThat(champ.getImageUrl()).isEqualTo(testImageUrl);
    }

    @Test
    public void testDeleteLolChampion() throws Exception {
        int testId = 2;

        MvcResult mvcResult = this.mockMvc.perform(delete(baseRestUrl + "/" + testId)
                .header("Content-Type","application/json"))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andReturn();

        // then do it all again to show it's not there!
        this.mockMvc.perform(delete(baseRestUrl + "/" + testId)
                        .header("Content-Type","application/json"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void testEditLolChampion() throws Exception {
        long testId = 1;

        String testDifficulty = "Hardest";
        String testName = "Test";
        String testRole = "Edit";
        Date testDate = new Date();
        String testImageUrl = "https://nourl/img.jpg";

        // create the edit object
        LolChampion testChamp = new LolChampion();
        testChamp.setId(testId);
        testChamp.setDifficulty(testDifficulty);
        testChamp.setName(testName);
        testChamp.setRole(testRole);
        testChamp.setDateCreated(testDate);
        testChamp.setImageUrl(testImageUrl);

        // write to JSON for put
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(testChamp);

        // do the edit
        MvcResult mvcResult = this.mockMvc.perform(put(baseRestUrl)
                        .header("Content-Type", "application/json")
                        .content(requestJson))
                        .andDo(print()).andExpect(status().isOk()).andReturn();

        // now read back the object response
        LolChampion champ =  new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<LolChampion>() {
                });

        // verify that the edits are in place
        assertThat(champ.getId()).isEqualTo(1);
        assertThat(champ.getDateCreated()).isEqualTo(testDate);
        assertThat(champ.getName()).isEqualTo(testName);
        assertThat(champ.getRole()).isEqualTo(testRole);
        assertThat(champ.getImageUrl()).isEqualTo(testImageUrl);


        // finally get the edited entry again
        mvcResult = this.mockMvc.perform(get(baseRestUrl + "/" + testId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // read in and test the results
        champ =  new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<LolChampion>() {
                });
        assertThat(champ.getId()).isEqualTo(1);
        assertThat(champ.getDateCreated()).isEqualTo(testDate);
        assertThat(champ.getName()).isEqualTo(testName);
        assertThat(champ.getRole()).isEqualTo(testRole);
        assertThat(champ.getImageUrl()).isEqualTo(testImageUrl);
    }
}

