package com.training.lolchampion.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LolChampionTest {

    private static final String testDifficulty = "Hardest";
    private static final String testName = "Test";
    private static final String testRole = "Edit";
    private static final Date testDate = new Date();
    private static final String testImageUrl = "https://nourl/img.jpg";
    private LolChampion champ;

    @BeforeEach
    public void setup() {this.champ = new LolChampion(); }

    @Test
    public void setTestDifficulty() {
        this.champ.setDifficulty(testDifficulty);
        assertEquals(testDifficulty,this.champ.getDifficulty());
    }

    @Test
    public void setTestName() {
        this.champ.setName(testName);
        assertEquals(testName,this.champ.getName());
    }

    @Test
    public void setTestRole() {
        this.champ.setRole(testRole);
        assertEquals(testRole,this.champ.getRole());
    }

    @Test
    public void setTestDate() {
        this.champ.setDateCreated(testDate);
        assertEquals(testDate,this.champ.getDateCreated());
    }

    @Test
    public void setTestImageUrl() {
        this.champ.setImageUrl(testImageUrl);
        assertEquals(testImageUrl,this.champ.getImageUrl());

    }
}
