CREATE TABLE LolChampion (
    id INT NOT NULL AUTO_INCREMENT,
    difficulty VARCHAR(255)  NOT NULL,
    name varchar(255) NOT NULL,
    role varchar(255) NOT NULL,
    dateCreated datetime DEFAULT NULL,
    imageUrl varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
    );

INSERT INTO LolChampion (id, difficulty, name, role) VALUES(1, 'Easy', 'Java', 'Developer');
INSERT INTO LolChampion (id, difficulty, name, role) VALUES(2, 'Medium', 'Angular', 'Developer');
INSERT INTO LolChampion (id, difficulty, name, role) VALUES(3, 'Hard', 'Cpp', 'Developer');